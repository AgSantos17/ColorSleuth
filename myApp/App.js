import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Alert, Image, View, TouchableNativeFeedback } from 'react-native';
import Start from './components/Start';
import Table from './components/Table';
import Ready from './components/Ready';
import Difficulty from './components/Difficulty';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

export default class FlexDirectionBasics extends Component {
  constructor(props) {
    super(props);
    }

  render() {
      return <RootStack />;
  }
};

const RootStack = StackNavigator(
  {
    Start: {
      screen: Start,
      navigationOptions: {
      header: null,
   }
    },
    Ready: {
      screen: Ready,
      navigationOptions: {
      header: null,
    }
        },
        Difficulty: {
          screen: Difficulty,
            },
    Table: {
      screen: Table,
      navigationOptions: ({ navigation }) => ({
      headerTitleStyle :{alignSelf:'center'},
      headerLeft:   <TouchableNativeFeedback onPress={() => navigation.replace('Difficulty')}>
                      <Image style={{marginLeft: 10, width: 25, height: 25}} source={require('./assets/arrow.png')} />
                    </TouchableNativeFeedback>,
   })
    },
  },
  { headerMode: 'screen' }
 );

// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => FlexDirectionBasics);
