import React, { Component } from 'react';
import { AppRegistry, BackHandler, View, StyleSheet, Text, TouchableOpacity, Alert, Button, TouchableNativeFeedback } from 'react-native';
import Table from './Table';
import Ready from './Ready';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

export class Start extends Component {
  constructor(props) {
    super(props);
      }

      handleBackButtonClick() {
      BackHandler.exitApp();
      return true;
    }

  render() {

    return (
      <View style={styles.container}>

      <Text style={styles.title} >ColorSleuth</Text>

        <View style={styles.button}>
          <Button title="Start" onPress={() => this.props.navigation.push('Difficulty')}/>
        </View>

        <View style={styles.button}>
          <Button title="Exit" onPress={this.handleBackButtonClick}/>
        </View>

        </View>

    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 40,
    fontWeight: 'bold',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    padding: 15,
    marginTop: 95,
    marginBottom: 25,
    top:0,
  },
  button: {
    width: 180,
    marginTop: 20,
  },
  buttContainer:{
    flex: 1,
    justifyContent: 'center',
  }
});


export default Start;
