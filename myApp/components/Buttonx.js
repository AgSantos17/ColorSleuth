import React, { Component } from 'react';

import {
  View,
  Text,
  TouchableNativeFeedback,
  StyleSheet,
  Alert,

} from 'react-native';

  export class Buttonx extends Component {
  constructor(props){
  super(props)
  }

  selectedButt(id){
    var id = this.props.id;
    this.props.checkCorrect(id);
  }

  render() {
    return (
      <TouchableNativeFeedback onPress={this.selectedButt.bind(this)}>
        <View style={styles.buttons} backgroundColor={this.props.color} id={this.props.id}/>
      </TouchableNativeFeedback>
    );
    }
    }

const styles = StyleSheet.create({
  buttons:{
    width: 110,
    height: 110,
    margin: 3,
  },
});

export default Buttonx;
