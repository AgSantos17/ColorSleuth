import React, { Component } from 'react';
import { AppRegistry, View, StyleSheet, Text, TouchableOpacity, Alert, Button, TouchableNativeFeedback } from 'react-native';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json
import CountdownCircle from 'react-native-countdown-circle';

export class Start extends Component {
  constructor(props) {
    super(props);
      }

  render() {
    return (
    <View style={styles.container}>
      <CountdownCircle
         seconds={3}
         radius={50}
         borderWidth={10}
         color="#ff003f"
         bgColor="#fff"
         textStyle={{ fontSize: 35 }}
         onTimeElapsed={() => this.props.navigation.navigate('Table')}
       />
    </View>

    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


export default Start;
