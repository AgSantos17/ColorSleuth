import React, { Component } from 'react';

import {
  View,
  Text,
  TouchableNativeFeedback,
  StyleSheet,
  Alert,
  Animated,

} from 'react-native';
import TimerMixin from 'react-timer-mixin';
import CountdownCircle from 'react-native-countdown-circle';
import Buttonx from './Buttonx';
import Player from './Player';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

export class Table extends Component {

  constructor(props) {
    super(props);
      this.state = {
        player1: 0,
        player2: 0,
        playerStatus: 1,
        timer: 0,
        difficulty: 0,
         }

      let { params } = this.props.navigation.state;
      this.seconds = params ? params.seconds : null;
      }

        componentDidMount(){
          TimerMixin.setTimeout(
            () => { console.log('I do not leak!'); },
            5000
          );

          this.setBoard();
         }

        setBoard = (e) => {

           var R = Math.floor(Math.random() * 236);
           var G = Math.floor(Math.random() * 236);
           var B = Math.floor(Math.random() * 236);
           var color = R+','+G+','+B;

           this.setState({
             button1: 'rgb('+R+','+G+','+B+')',
             button2: 'rgb('+R+','+G+','+B+')',
             button3: 'rgb('+R+','+G+','+B+')',
             button4: 'rgb('+R+','+G+','+B+')',
           });

           array = [1,2,3,4]
           const random = array[Math.floor(Math.random()*array.length)].toString();

             var stateObject = function() {
                 var obj = {}
                 R = R+20;
                 G = G+20;
                 B = B+20;
                 var diffColor = R+','+G+','+B;

                 obj['button'+random] = 'rgb('+R+','+G+','+B+')'

                    return obj;

               }.bind(this)();

               this.setState({ ...stateObject, id:random },
                 () => {
                          // Alert.alert(JSON.stringify('The right answer is '+Object.keys(stateObject)[0]));
                      });
          }

          checkCorrect = (id) => {

            var win = this.state.id;

            var stateObject = function() {
                  var obj = {}
                  var status = this.state.playerStatus;
                  var stat1 = this.state.player1;
                  var stat2 = this.state.player2;
                  const num = status.toString();

                    if(win === id){
                      obj['player1'] = stat1 + 1;
                      return obj;
                    }

                    else{
                      obj['player1'] = stat1 - 1;
                      return obj;
                    }

                 }.bind(this)();


              this.setState({ ...stateObject });

            this.setBoard();
          }


  render() {

    return (

      <View style={styles.container}>

        <View style={{marginLeft:80, flex:1, alignSelf:'center',position: 'absolute', top:145, zIndex: 999999}}>
                      <CountdownCircle
                         seconds={this.seconds}
                         radius={20}
                         borderWidth={5}
                         color="#ff003f"
                         bgColor="#fff"
                         textStyle={{ fontSize: 18 }}
                         onTimeElapsed={() => Alert.alert(
                                            'Score',
                                            this.state.player1.toString(),
                                            [
                                              {text: 'Exit', onPress: () => this.props.navigation.popToTop()},
                                              {text: 'Retry', onPress: () => this.props.navigation.replace('Table', {
                                                seconds: this.seconds,
                                              })},
                                            ],
                                            { cancelable: false }
                                          )}
                       />
                     </View>

        <View style={styles.buttonContainer} alignItems='flex-end'>

          <Buttonx color={this.state.button1} id='1' checkCorrect={this.checkCorrect}/>

          <Buttonx color={this.state.button2} id='2' checkCorrect={this.checkCorrect}/>

        </View>

        <View style={styles.buttonContainer}>

          <Buttonx color={this.state.button3} id='3' checkCorrect={this.checkCorrect}/>

          <Buttonx color={this.state.button4} id='4' checkCorrect={this.checkCorrect}/>

        </View>

          <View style={styles.buttonContainer}>
            <Player name="Score" score={this.state.player1}  pColor = {this.state.playerStatus === 1 ? '#cecece' : '#e4e4e4' } />
            {/* <Player name="Player 2" score={this.state.player2}  pColor = {this.state.playerStatus === 2 ? '#cecece' : '#e4e4e4' }/> */}
          </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
  },
  player: {
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
  }
});

export default Table;
