import React, { Component } from 'react';
import { AppRegistry, BackHandler, View, StyleSheet, Text, TouchableOpacity, Alert, Button, TouchableNativeFeedback } from 'react-native';
import Table from './Table';
import Ready from './Ready';
import DiffButt from './DiffButt';
import { StackNavigator } from 'react-navigation'; // Version can be specified in package.json

export class Difficulty extends Component {
  constructor(props) {
    super(props);
      }

      handleBackButtonClick() {
      BackHandler.exitApp();
      return true;

    }

  render() {
    return (
      <View style={styles.container}>

      <Text style={styles.title} >Difficulty</Text>

        <DiffButt name="Easy" seconds="15" to="Table"/>

        <DiffButt name="Normal" seconds="10" to="Table"/>

        <DiffButt name="Hard" seconds="5" to="Table"/>

        </View>

    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 40,
    fontWeight: 'bold',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    padding: 15,
    marginTop: 35,
    top:0,
  },
  button: {
    width: 180,
    marginTop: 30,
  },
  buttContainer:{
    flex: 1,
    justifyContent: 'center',
  }
});


export default Difficulty;
