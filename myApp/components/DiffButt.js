import React, { Component } from 'react';

import {
  View,
  Button,
  StyleSheet,

} from 'react-native';
import { withNavigation } from 'react-navigation';

  export class DiffButt extends Component {
  constructor(props){
  super(props)
  }

  render() {
    return (
      <View style={styles.button} >
        <Button title={this.props.name} onPress={() => { this.props.navigation.replace(this.props.to, {
              seconds: this.props.seconds,
            });
          }} />
      </View>
    );
    }
    }

const styles = StyleSheet.create({
  button: {
    width: 180,
    marginTop: 30,
  },
});

export default withNavigation(DiffButt);
