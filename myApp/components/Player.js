import React, { Component } from 'react';

import {

  View,
  Text,
  StyleSheet,

} from 'react-native';

  export class Player extends Component {
  constructor(props){
  super(props)
  }

  render() {
    return (
        <View style={styles.players} backgroundColor={this.props.pColor}>
          <Text>{this.props.name}</Text>
          <Text>{this.props.score}</Text>
        </View>
    );
    }
    }

const styles = StyleSheet.create({
  players:{
    width: 120,
    height: 100,
    backgroundColor: '#e4e4e4',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Player;
